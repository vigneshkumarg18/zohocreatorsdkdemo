//
//  AppDelegate.swift
//  DemoProject
//
//  Created by Vigneshkumar G on 17/10/18.
//  Copyright © 2018 viki. All rights reserved.
//

import UIKit
import ZohoCreatorSDK
import ZMLKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let scope = [ "ZohoCreator.meta.READ","ZohoCreator.data.READ","ZohoCreator.meta.CREATE","ZohoCreator.data.CREATE","ZohoCRM.modules.leads.READ","ZohoCRM.modules.accounts.READ","ZohoCRM.modules.deals.READ","ZohoCRM.modules.contacts.READ","ZohoCRM.modules.campaigns.READ","ZohoCRM.modules.cases.READ","ZohoCRM.modules.solutions.READ","ZohoCRM.modules.products.READ","ZohoCRM.modules.pricebooks.READ","ZohoCRM.modules.quotes.READ","ZohoCRM.modules.vendors.READ","ZohoCRM.modules.purchaseorders.READ","ZohoCRM.modules.salesorders.READ","ZohoCRM.modules.invoices.READ","ZOHOCRM.users.READ","ZohoCRM.crmapi.ALL","aaaserver.profile.READ","ZohoContacts.userphoto.READ","ZohoContacts.contactapi.READ" ]
        
        let clientID = "1000.GGPIAQ0USVTU61348S27HXP51ERNYU"
        
        let clientSecret = "a18b1a45d722b4b1dc42eb43ef78afcb1605a9f343"
        
        let urlScheme = "creatorsdk"
        
        let accountsUrl = "https://accounts.zoho.com"
        
        ZohoAuth.initWithClientID(clientID, clientSecret: clientSecret, scope: scope, urlScheme: urlScheme, mainWindow: self.window!, accountsURL: accountsUrl)
        
        ZohoAuth.getOauth2Token { (token, error) in
            if token == nil{
                ZohoAuth.presentZohoSign(in: { (token, error) in
                    if token != nil{
                        // success login                                
                    }
                })
            }else{
                // success login
            }
        }
        
        // Creator SDK Configuration
        
        Creator.configure(authService: self)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let sourceApplication = options[UIApplication.OpenURLOptionsKey.sourceApplication] as! String
        let annotation = options[UIApplication.OpenURLOptionsKey.annotation]
        ZohoAuth.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ZohoAuth.handleURL(url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    
}



extension AppDelegate: AuthenticationProtocol
{
    func oAuthToken(with completion: @escaping tokenCompletion) {
        
    }
    
    func zuid() -> String {
        return ""
    }
    
    func configuration() -> CreatorConfiguration {
        return CreatorConfiguration(creatorURL: "https://creator.zoho.com")
    }
    
    func getAccessToken(with completion: @escaping tokenCompletion) {
        ZohoAuth.getOauth2Token { (token, error) in
            completion(token,error)
        }
    }
}




struct Component: ComponentProtocol {
    var linkName: String
    
    var appOwner: String
    
    var appLinkName: String
    
    var displayName: String
}
