//
//  ViewController.swift
//  DemoProject
//
//  Created by Vigneshkumar G on 17/10/18.
//  Copyright © 2018 viki. All rights reserved.
//

import UIKit
import ZohoCreatorSDK

class ViewController: UIViewController {
    @IBOutlet weak var appLinknameField: UITextField!
    @IBOutlet weak var appOwnerField: UITextField!
    @IBOutlet weak var formLinkField: UITextField!
    @IBOutlet weak var reportLinkNamefield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    @IBAction func fetchApps(_ sender: Any) {
        Creator.fetchAppList { (result) in
            self.print(result: result)
        }
    }
    
    
    @IBAction func fetchSections(_ sender: Any) {
        let app = Application(appOwnerName: appOwnerField.text!, appLinkName: appLinknameField.text!)
        Creator.fetchSectionList(for: app) { (result) in
          self.print(result: result)
        }
    }
    @IBAction func fetchForm(_ sender: Any) {
        let component = Component.init(linkName: formLinkField.text!, appOwner: appOwnerField.text!, appLinkName: appLinknameField.text!, displayName: "")
        Creator.FormModule.fetchLiveForm(component: component, executeWorkFlow: false) { (result) in
            self.print(result: result)
        }
    }
    @IBAction func fetchReport(_ sender: Any) {
        let component = Component.init(linkName: reportLinkNamefield.text!, appOwner: appOwnerField.text!, appLinkName: appLinknameField.text!, displayName: "")
        Creator.ReportModule.fetchListReport(from: .init(component: component
        )) { (result) in
           self.print(result: result)
        }
    }
    
    
    func print<T>(result: Result<T>)  {
        switch  result {
        case .failure(let error):
            dump(error)
        case .sucess(let value):
            dump(value)
        }
    }
}


